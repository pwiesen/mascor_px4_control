# -*- coding: latin-1 -*-

###
#   Copyright (c) 2020 FH Aachen. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

"""A PX4 library to make life easier with MAVROS.

MAV is the main class of the mascor_px4_control library.
It initializes all necessary subhandlers to get full control over
a MAV.

    Typical usage example:

    mav = MAV()
    mav.command.takeoff(5)
    position = mav.localization.get_position()
    mav.setpoint.position(position, yaw=1.57)
    mav.command.set_offboard()
"""

import threading

import rospy

## -- Submodules
from loghandler import LogHandler
from localizationhandler import LocalizationHandler
from setpointhandler import SetpointHandler
from statehandler import StateHandler
from commandhandler import CommandHandler
from preflighthandler import PreflightHandler
from missionhandler import MissionHandler


class MAV:
    mavCount = 0

    def __init__(self, namespace):
        """MAV class.
        
        Attributes:
            namespace (str): namespace of mavros instance. Leave empty for one mavros instance.
        """
        MAV.mavCount += 1
        self.__killed = False
        self.__namespace = namespace
        self.__mav_id = MAV.mavCount

        ## -- Handler-Initialization
        self.logger = LogHandler(self.__mav_id)
        self.preflight = PreflightHandler(self.__namespace, self.logger)
        self.state = StateHandler(self.__namespace, self.logger)
        self.localization = LocalizationHandler(self.__namespace, self.logger)
        self.mission = MissionHandler(self.__namespace)
        self.setpoint = SetpointHandler(self.__namespace, self.localization, self.logger)
        self.command = CommandHandler(self.__namespace, self.localization, self.state, self.logger)

        ## Destructor
        rospy.on_shutdown(self.delete)
        ## Preflightcheck
        if self.preflight.check():
            self.setpoint.start_thread()

    ## -- DESTRUCTOR
    def delete(self):
        """Destructor
        
        Will land MAV at actual position before killing threads.
        """
        try:
            if not self.state.landed():
                self.command.land()
            self.setpoint.kill_thread()
            self.command.kill_thread()
            self.__killed = True
            del self.command
            del self.setpoint
            del self.mission
            del self.localization
            del self.state
            del self.preflight
            self.logger.loginfo("[DELETE] ✓")
            del self.logger
        except AttributeError:
            pass
        MAV.mavCount -= 1

    ## -- GET FUNCTIONS
    def get_id(self):
        """Get ID of MAV

        Returns:
            int: MAV id
        """
        return self.__mav_id

    def get_namespace(self):
        """Get namespace of MAV

        Returns:
            str: namespace
        """
        return self.__namespace

    def is_killed(self):
        return self.__killed
