# -*- coding: latin-1 -*-

###
#   Copyright (c) 2020 FH Aachen. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
###

import thread

from mascor_px4_control import geonav_conversions as gc

import rospy
import tf2_ros
import tf
from geometry_msgs.msg import TransformStamped, Pose 


class FrameSynchronizer:
    def __init__(self, mavs, origin, origin_frame="world"):
        self.origin_frame = origin_frame
        self.origin = origin
        self.mavs = mavs
        self.kill_thread = False
        # TF
        self.tfBuffer = tf2_ros.Buffer()
        self.tfListener = tf2_ros.TransformListener(self.tfBuffer)
        self.br = tf2_ros.TransformBroadcaster()

        rospy.on_shutdown(self.kill)

        # Main
        try:
            thread.start_new_thread(self.main, ())
        except:
            rospy.logerr("Error: Unable to start thread 'setpointRAW'")

    def add_mav(self, mav):
        self.mavs.append(mav)

    def generate_gp_transform(self, mav, namespace):
        trans = TransformStamped()
        trans.header.stamp = rospy.Time.now()
        trans.header.frame_id = self.origin_frame
        trans.child_frame_id = namespace
        gp_msg = mav.localization.get_gp_msg()
        lp_msg = mav.localization.get_lp_msg()
        x, y = gc.ll2xy(gp_msg.latitude, gp_msg.longitude, self.origin[0], self.origin[1])
        trans.transform.translation.x = x
        trans.transform.translation.y = y
        trans.transform.translation.z = gp_msg.altitude - self.origin[2]
        trans.transform.rotation = lp_msg.pose.orientation
        return trans

    def generate_lp_transform(self, mav, namespace):
        lp_msg = mav.localization.get_lp_msg()
        trans = TransformStamped()
        trans.header.stamp = rospy.Time.now()
        trans.header.frame_id = namespace + "_origin"
        trans.child_frame_id = namespace + "_helper"
        trans.transform.translation.x = lp_msg.pose.position.x
        trans.transform.translation.y = lp_msg.pose.position.y
        trans.transform.translation.z = lp_msg.pose.position.z
        trans.transform.rotation = lp_msg.pose.orientation
        return trans

    def lookup_invers(self, trans_lp, namespace):
        try:
            trans_inv = self.tfBuffer.lookup_transform(trans_lp.child_frame_id, trans_lp.header.frame_id, rospy.Time(0))
            trans_inv.header.frame_id = namespace
            trans_inv.child_frame_id = namespace + "_origin"
            if not trans_inv.transform.rotation.w == 0.0:
               return trans_inv 
        except:
            rospy.logwarn("Lookup failed.")
        return None

    def kill(self):
        self.kill_thread = True

    def main(self):
        rate = rospy.Rate(50)
        while not rospy.is_shutdown():
            if self.kill_thread:
                break
            for mav in self.mavs:
                if mav.is_killed():
                    continue
                if mav.localization.has_gp_fix() and mav.localization.has_lp_fix():
                    namespace = mav.get_namespace()
                    if namespace == "":
                        namespace = "uav" 
                    trans_gp = self.generate_gp_transform(mav, namespace)
                    try:
                        self.br.sendTransform(trans_gp)
                    except:
                        pass
                    trans_lp = self.generate_lp_transform(mav, namespace)
                    try:
                        self.br.sendTransform(trans_lp)
                    except:
                        pass
                    trans_inv = self.lookup_invers(trans_lp, namespace)
                    if trans_inv is not None:
                        try:
                            self.br.sendTransform(trans_inv)
                        except:
                            pass
            try:
                rate.sleep()
            except rospy.exceptions.ROSInterruptException:
                pass

