# mascor_px4_control library
## author
Tobias Müller, B.Eng. - University of Applied Sciences Aachen
mail: tobias.mueller@fh-aachen.de
## info
This libray should simplify the control of the PX4 stack written in Python.

## included
- library (python)
- library testscript (lib_test.py)

## functions
- safety zone
- arm/disarm
- takeoff/land
- return to land
- go to position
- position queue
- position control
- position control + velocity feedforward
- velocity control (vx,vy,vz)/(vx,vy,z)

## howto

- simply import the library into your script with

```
from mascor_px4_control import MAV
```

- have a look into lib_test.py for examples

## todo (WIP)
- LICENCES file
- yaw to next position
- better multithreading

## roadmap
- handle px4 new offboard plans
- c++ implementation
